<style type="text/css">
	.key {
	    height: 50px;
	    width: auto !important;
	    display: inline-block;
	    margin-right: 55px;
	}

	.key_head {
	    display: inline-block;
	}
</style>

<main>
	<section class="product_page" >

        <!-- Header -->
        <header>
            <a href="/"><img src="<?=FRONT_ASSETS?>img/webair.png"></a>
            <a class='arrow_back' href="/home/products"><img src="<?=FRONT_ASSETS?>img/arrow.png"></a>
        </header>

        <div class='banner'>
        	<div class='banner_overlay'>
        		<img style='height: 111px;' src="<?=FRONT_ASSETS?>img/disaster_logo.png">
        		<h1>Disaster Recovery And Off-Site Backup Solutions</h1>
        		<div>
        			<!-- <img src="<?=FRONT_ASSETS?>img/about_logo.png"> -->
        		</div>
        	</div>
        </div>

        <div class='content'>
        	<h2>Fully Managed Disaster Recovery Solutions</h2>
        	<p>Webair offers proven, tested and fully managed Disaster Recovery services (DR) and Off-Site Backup Solutions to safeguard your mission-critical data and applications. Webair’s DRaaS and Off-Site Backup Solutions provide customers the ability to recover from the threat of data loss via replication of the entire production environment on dedicated, secure and reliable off-site infrastructure. This allows customers to rapidly restore mission-critical data and applications in the face of an unanticipated man-made or natural event.</p>
        </div>

        <div class='darken' style="background-image: url('<?=FRONT_ASSETS?>img/disaster_back.png')">
        	<h2>Notable Vendor in Gartner's 2017 Magic Quadrant for Disaster Recovery-as-a-Service.</h2>
        </div>

        <div class='content'>
        	<img class=' key' src="<?=FRONT_ASSETS?>img/disaster_key.png"><h2 class='key_head'>Key Advantages</h2>
        	<p>Webair’s Cloud-based Disaster Recovery and Off-Site Backup Solutions</p>
        	<h3>Customized DR Plans</h3>
        	<p style='margin-top: 0;'>Pre-planned, pre-configured and customized per deployment</p>
        	<h3>Multi-Platform Support</h3>
        	<p style='margin-top: 0;'>Compatibility with VMware, Hyper-V, IBM platforms, physical servers, Azure and AWS</p>
        	<h3>Data Centers Worldwide</h3>
        	<p style='margin-top: 0;'>Recovery sites located in New York, Los Angeles, Montrèal, Singapore, and Amsterdam as well as Microsoft Azure regions</p>
        	<h3>Fully Managed Disaster Recovery</h3>
        	<p style='margin-top: 0;'>Full ownership and accountability for the entire disaster recovery process including failover, failback and testing</p>
        </div>
    </section>
</main>

 