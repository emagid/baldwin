<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 11/9/15
 * Time: 6:28 PM
 */
namespace Model;

class Wishlist extends \Emagid\Core\Model {

    static $tablename = 'wishlist';

    public static $fields =  [
        'user_id',
        'data',
        'insert_time',
        'active'
    ];

    public static function getProductsByUserId($id){
        return Product::getList(['where'=>'product.id in (select wishlist.product_id from '.self::$tablename.' where wishlist.user_id = '.$id.' and wishlist.active = 1)']);
    }

    public function product()
    {
        $product = new Product_Map();
        $productData = json_decode($this->data);
        $product = $product->getProduct($productData->product_id,$productData->product_type_id);
//        return Product::getItem(null, ['where' => 'id ='.$this->product_id]);
        return $product;
    }

}