<?php

class homeController extends siteController {
        function __construct(){
        parent::__construct();
    }    
    public function index(Array $params = []){


        $this->viewData->banners = \Model\Banner::getList(['where'=>"active='1'"]);
        $this->viewData->mainBanner = \Model\Banner::getList(['where'=>"active = 1 and featured_id = 0", 'orderBy'=>"banner_order asc"]);
        $this->viewData->jewelry = \Model\Jewelry::getList(['where' => "active = 1 and featured = 1 and discount != 0", 'orderBy' => "RANDOM()", 'limit' => 2]);

        $this->viewData->diamond = \Model\Product::getItem(null, ['where' => "active = 1 and featured = 1 and discount != 0 and quantity > 0", 'orderBy' => "RANDOM()", 'limit' => 1]);
        $this->viewData->ring = \Model\Ring::getItem(null, ['where' => "active = 1 and featured = 1 and discount != 0", 'orderBy' => "RANDOM()", 'limit' => 1]);
        
        $this->loadView($this->viewData);
    }

    function photobooth(Array $params = []){
        $this->loadView($this->viewData);
    }

    function about(Array $params = []){
        $this->loadView($this->viewData);
    }

    function answer(Array $params = []){
        $this->loadView($this->viewData);
    }


    function products(Array $params = []){
        $this->loadView($this->viewData);
    }

    function security(Array $params = []){
        $this->loadView($this->viewData);
    }


    function trivia(Array $params = []){
        $this->loadView($this->viewData);
    }

    function healthcare(Array $params = []){
        $this->loadView($this->viewData);
    }

    function privatecloud(Array $params = []){
        $this->loadView($this->viewData);
    }

    function hybrid(Array $params = []){
        $this->loadView($this->viewData);
    }

    function network(Array $params = []){
        $this->loadView($this->viewData);
    }

    function management(Array $params = []){
        $this->loadView($this->viewData);
    }

    function disaster(Array $params = []){
        $this->loadView($this->viewData);
    }

    function ransomeware(Array $params = []){
        $this->loadView($this->viewData);
    }

    function ny1(Array $params = []){
        $this->loadView($this->viewData);
    }
}